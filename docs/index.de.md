# Willkommen

![Logo – eine Katze](images/logo.png){ align=right }

!!! tip ""
	Docker Dokumentationen ohne Probleme.

[ekleinod/mkdocs-material](https://hub.docker.com/r/ekleinod/mkdocs-material) ist ein Docker Image für [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

Ich brauche das Image, um über verschiedene Rechner und Betriebssysteme eine Umgebung für Dokumentationen zu haben.
Das bedeutet: mkdocs und mkdocs-material und die für meine Projekte notwendigen Plugins/Extensions.

Diese Dokumentation ist nicht umfassend, sondern eine Gedankenstütze für mich, wie ich *mkdocs-material* anpasse.
Und für [Links](99_links.md) zu den wichtigen Seiten.

Nutze [das Template](https://gitlab.com/etg-docker/mkdocs-material/-/tree/main/template) :smile:
