# Links

- [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)
- [PyMdown Extensions](https://facelessuser.github.io/pymdown-extensions/)
- Markdown
	- [Markdown Syntax](https://daringfireball.net/projects/markdown/syntax)
	- [MkDocs Docmark - Cheat sheet](https://yakworks.github.io/docmark/cheat-sheet/)
	- [Markdown Extensions](https://python-markdown.github.io/extensions/)
	- [Markdown Extensions Configuration](https://www.mkdocs.org/user-guide/configuration/#markdown_extensions)
- Syntax Highlighting
	- [code blocks](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/)
	- [pygments](https://pygments.org)
	- [languages](https://pygments.org/languages/)
- Emojis
	- [Emojipedia](https://emojipedia.org/twitter/)
- [i18n](https://ultrabug.github.io/mkdocs-static-i18n/setup/) (translations)
