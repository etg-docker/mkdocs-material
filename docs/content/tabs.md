# Tabs

- [Content Tabs](https://squidfunk.github.io/mkdocs-material/reference/content-tabs/)
- [Tabbed](https://facelessuser.github.io/pymdown-extensions/extensions/tabbed/)

~~~ yaml title="mkdocs.yml"
markdown_extensions:
  - pymdownx.superfences
  - pymdownx.tabbed:
      alternate_style: true
~~~

=== "First tab"

	and some content

=== "Second tab"

	more content

	- even a list
	- with entries

After the tabs...

~~~~ markdown title="markdown"
=== "First tab"

	and some content

=== "Second tab"

	more content

	- even a list
	- with entries
~~~~
