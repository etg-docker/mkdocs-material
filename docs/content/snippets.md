# Snippets (and Abbreviations and a Glossary)

- [Snippets](https://facelessuser.github.io/pymdown-extensions/extensions/snippets/)
- [Abbreviations](https://squidfunk.github.io/mkdocs-material/reference/tooltips/#adding-abbreviations)
- [Glossary](https://squidfunk.github.io/mkdocs-material/reference/tooltips/#adding-a-glossary)

~~~ yaml title="mkdocs.yml"
markdown_extensions:
  - abbr
  - pymdownx.snippets:
      base_path: ["docs/snippets"]
      check_paths: true
      auto_append: ["glossary.md"]
exclude_docs: |
  snippets/
~~~

## Goals

- reusable content
- central abbreviation list for glossary

## Reuse content

- save snippet in own file
- use separate folder in order to not create pages for every snippet, e.g. "snippets"
- declare base path to docs (state snippet file name from here)
- exclude snippet folder from docs content to avoid doc generation for snippets
- check paths in order to fail build process if a snippet is not found

=== "Result"

	--8<-- "nice.md"

=== "Markdown"

	~~~~ markdown
	;--8<-- "nice.md"
	~~~~

## Glossary

- create glossary snippet
- fill with abbreviations
- declare as autoloaded file (will be appended to *every* documentation page)
- examples: HTML, XML, mkdocs-material
