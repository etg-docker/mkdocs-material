# Code Blocks

- [Code Blocks](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/)
- [Pygments](https://pygments.org)
- [Languages](https://pygments.org/languages/)

~~~ yaml title="mkdocs.yml"
markdown_extensions:
  - pymdownx.highlight:
      use_pygments: true
~~~

=== "Result"

	~~~ yaml title="mkdocs.yml"
	markdown_extensions:
		- pymdownx.highlight:
				use_pygments: true
	~~~

=== "Markdown"

	~~~~ markdown
	~~~ yaml title="mkdocs.yml"
	markdown_extensions:
		- pymdownx.highlight:
				use_pygments: true
	~~~
	~~~~
