# Keyboard keys

!!! bug "Lamento"
	Unfortunately, menus are still missing.

- [Keys](https://facelessuser.github.io/pymdown-extensions/extensions/keys/)

~~~ yaml title="mkdocs.yml"
markdown_extensions:
  - pymdownx.keys
~~~

=== "Result"

	Press ++ctrl+alt+delete++ to continue...
	And don't forget the ++"anykey"++.

=== "Markdown"

	~~~~ markdown
	Press ++ctrl+alt+delete++ to continue...
	And don't forget the ++"anykey"++.
	~~~~

## Cool keys

Name | Output
---|---
bar | ++bar++
arrow-up | ++arrow-up++
page-down | ++page-down++
home | ++home++
end | ++end++
tab | ++tab++
backspace | ++backspace++
delete | ++delete++
insert | ++insert++
eject | ++eject++
enter | ++enter++
escape | ++escape++
shift | ++shift++
windows | ++windows++
context-menu | ++context-menu++
fingerprint | ++fingerprint++
