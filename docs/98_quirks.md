# Quirks

- no automatic link replacement when moving files (but link warnings)

## Automatic Navigation

- `index.md` at top
- rest sorted by filename
- use number scheme if order is relevant
