#!/bin/bash

CONTAINERNAME=mkdocs-material
IMAGENAME=ekleinod/$CONTAINERNAME

echo "Build documentation to public."
echo

docker run \
	--rm \
	--name $CONTAINERNAME \
	--user "$(id -u):$(id -g)" \
	--volume "${PWD}:/docs" \
	$IMAGENAME \
		build --strict --verbose \
		$@
