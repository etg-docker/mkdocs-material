#!/bin/bash

source ../settings.sh

echo "Build image $IMAGENAME"
echo

docker build \
	--progress=plain \
	--tag $IMAGENAME:$DOCKER_TAG_CURRENT \
	--build-arg IMAGE_VERSION=$MATERIAL_VERSION \
	.

docker tag $IMAGENAME:$DOCKER_TAG_CURRENT $IMAGENAME:$DOCKER_TAG_LATEST
docker tag $IMAGENAME:$DOCKER_TAG_CURRENT $IMAGENAME:$DOCKER_TAG_INTERNAL
